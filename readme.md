#Модуль оплаты платежной системы IntellectMoney для CMS WebAsyst (ShopScript)

> **Внимание!** <br>
Данная версия актуальна на *5 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557071#557071b323bfbdecf74c8a8969537eea69b57d.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557071#5570713452655b56cf4878badb3178d09876bf
<br>
Ответы на частые вопросы можно найти здесь: https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557071#55707163495f8bca704d02b0590ce4974dc084
