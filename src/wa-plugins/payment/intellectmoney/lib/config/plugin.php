<?php
return array(
    'name'        => 'IntellectMoney',
    'description' => 'Платежная система <a href="http://intellectmoney.ru/">IntellectMoney</a>',
    'icon'        => 'img/favicon.ico',
    'logo'        => 'img/intellectMoney.png',
    'vendor'      => '1017979',
    'version'     => '6.3.1',
    'locale'      => array('ru_RU'),
    'type'        => waPayment::TYPE_ONLINE,
);
