<?php

require_once("wa-plugins/payment/intellectmoney/lib/intellectmoney/IntellectMoneyCommon/LanguageHelper.php");
require_once("wa-plugins/payment/intellectmoney/lib/intellectmoney/intellectmoneyAdminController.php");

$adminController = new intellectmoneyAdminController();
return $adminController->getParams();