<?php

require_once("intellectmoney/IntellectMoneyCommon/UserSettings.php");
require_once("intellectmoney/IntellectMoneyCommon/Order.php");
require_once("intellectmoney/IntellectMoneyCommon/Customer.php");
require_once("intellectmoney/IntellectMoneyCommon/Payment.php");
require_once("intellectmoney/IntellectMoneyCommon/Status.php");
require_once("intellectmoney/IntellectMoneyCommon/Result.php");
require_once("intellectmoney/IntellectMoneyCommon/Currency.php");

class intellectmoneyPayment extends waPayment implements waIPayment {

    public function allowedCurrency() {
        return array(\PaySystem\Currency::RUB, \PaySystem\Currency::TST);
    }

    public function payment($payment_form_data, $order_data, $auto_submit = false) {
        $imUserSettings = $this->getUserSettings();
        $order = waOrder::factory($order_data);
        $imOrder = \PaySystem\Order::getInstance($order->data["params"]["invoiceId"], $order->id, $order->total, $order->total, 0, $order->shipping, $order->currency, $order->discount, NULL);

        $imCustomer = \PaySystem\Customer::getInstance($order->getContact()->get('email', 'default'), $order->getContact()->get('name', 'default'), $order->getContact()->get('phone', 'default'));

        $imPayment = \PaySystem\Payment::getInstance($imUserSettings, $imOrder, $imCustomer);

        $view = wa()->getView();
        $view->assign('form_url', $imUserSettings->getMerchantUrl());
        $view->assign('hidden_fields', $imPayment->getParamsForP2P());
        $view->assign('auto_submit', $auto_submit);
        return $view->fetch($this->path . '/templates/payment.html');
    }

    private function getUserSettings() {
        $pluginSettings = array();
        $imUserSettings = PaySystem\UserSettings::getInstance();
        foreach ($imUserSettings->getNamesOfP2PParamsToSave() as $param) {
            $pluginSettings[$param] = $this->$param;
        }
        $pluginSettings["userFields"] = array(
            "UserFieldName_1" => "app_id",
            "UserField_1" => $this->app_id,
            "UserFieldName_2" => "merchant_id",
            "UserField_2" => $this->merchant_id
        );

        $imUserSettings->setParams($pluginSettings);
        $imUserSettings->setMerchantUrl('https://merchant.intellectmoney.ru');
        return $imUserSettings;
    }

    protected function callbackInit($request) {
        //При установке этих значений становятся доступны настройки модуля из $this
        $this->app_id = $_REQUEST['UserField_1'];
        $this->merchant_id = $_REQUEST['UserField_2'];
        return parent::callbackInit($request);
    }

    protected function callbackHandler($request) {
        $order = shopPayment::getOrderData($request['orderId'], $this);
        if (empty($order)) {
            if ($_REQUEST['paymentStatus'] == \PaySystem\Status::paid) {
                throw new waException("Поступила оплата по удалённому заказу номер " . $_REQUEST['orderId'] . ". Номер счета в системе IntellectMoney - " . $_REQUEST['paymentId']);
            } else {
                throw new waException('OK');
            }
            die;
        }
        $imUserSettings = $this->getUserSettings();

        $imOrder = \PaySystem\Order::getInstance($request['paymentId'], $order->id, $order->total, $order->total, 0, $order->shipping, $order->currency, 0, $order->data["params"]["statusIM"]);

        $imResult = \PaySystem\Result::getInstance($request, $imUserSettings, $imOrder);
        $response = $imResult->processingResponse();

        if ($response->changeStatusResult) {
            if (empty($order->data["params"]["invoiceId"])){
                shopPayment::setOrderParams($orderId, ["invoiceId" => $invoiceId]);
            }
            shopPayment::setOrderParams($orderId, ["statusIM" => $statusIM]);
            $transaction_data = $this->formalizeData($request);	

            $callback_method = null;
            switch (ifset($transaction_data['state'])) {
                case self::STATE_CAPTURED:
                    $callback_method = self::CALLBACK_PAYMENT;
                    break;
            }
            
                $transaction_data = $this->saveTransaction($transaction_data, $request);
               $this->execAppCallback($callback_method, $transaction_data);
        }
        else {
            throw new waException($imResult->getMessage());
        }
        
        return $transaction_data;
    }
    
        protected function formalizeData($transaction_raw_data){
        $transaction_data = parent::formalizeData($transaction_raw_data);

        $fields = array(
            'userName'  => 'Имя Пользователя в Системе IntellectMoney',
            'userEmail' => 'Email Пользователя в Системе IntellectMoney'
        );
        foreach ($fields as $field => $description) {
            if (ifset($transaction_raw_data[$field])) {
                $view_data[] = $description.': '.$transaction_raw_data[$field];
            }
        }

        $transaction_data = array_merge($transaction_data, array(
            'type'        => null,
            'native_id'   => ifset($transaction_raw_data['paymentId']),
            'amount'      => ifset($transaction_raw_data['recipientAmount']),
            'currency_id' => ifset($transaction_raw_data['recipientCurrency']),
            'result'      => 1,
            'order_id'    => $transaction_raw_data['orderId'],
            'view_data'   => implode("\n", $view_data),
        ));

        switch (ifset($transaction_raw_data['paymentStatus'])) {
            case 3:
                $transaction_data['state'] = self::STATE_AUTH;
                $transaction_data['type'] = self::OPERATION_AUTH_ONLY;
                break;
            case 5:
                $transaction_data['state'] = self::STATE_CAPTURED;
                $transaction_data['type'] = self::OPERATION_CAPTURE;
                break;
        }
        return $transaction_data;
    }

}
