<?php

namespace PaySystem;

class IntellectMoneyBase {

    protected function convertToPriceFormat($amount) {
        return number_format($amount, 2, '.', '');
    }

}
