<?php

class intellectmoneyAdminController {
    private $imLanguageHelper;
    
    public function __construct() {
        $this->imLanguageHelper = PaySystem\LanguageHelper::getInstance();
    }
    
    public function getParams() {
        $result = array();
        
        $paramsForInputTypeText = array(
            'accountId',
            'formId',
            'secretKey',
            'expireDate',
            'successUrl',
            'preference'
        );
        foreach($paramsForInputTypeText as $param) {
            $result[$param] = $this->addParam($param, waHtmlControl::INPUT);
        }
        
        $paramsForSelect = array(
            'formType'
        );
        foreach($paramsForSelect as $param) {
            $result[$param] = $this->addParam($param, waHtmlControl::SELECT, array(__CLASS__, "getFormTypes"));
        }
        
        return $result;
    }
    
    private function addParam($param, $type, $optionsCallback = NULL) {
        return array(
            'value'        => '',
            'title'        => $this->imLanguageHelper->getTitle($param),
            'description'  => $this->imLanguageHelper->getDesc($param),
            'control_type' => $type,
            'options_callback' => $optionsCallback
        );
    }
    
    public static function getFormTypes() {
        return array(
            'IMAccount' => 'В личный кабинет',
            'PeerToPeer' => 'На карту'
        );
    }
}

?>